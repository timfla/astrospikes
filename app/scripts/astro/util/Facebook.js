can.Construct('astro.util.Facebook', {}, {
    init: function() {

    },

    sortByDob: function(friends) {
        if(!can.isArray(friends)) {
            return [];
        }

        var sortedFriends = friends;

        sortedFriends.sort(function(a, b) {
            return (a.dob.month * 31 + a.dob.day + a.sign * 1000) - (b.dob.month * 31 + b.dob.day + b.sign * 1000);
        });

        return sortedFriends;
    },

    extractDob: function(dob) {
        var day, month;
        if(dob && typeof dob === 'string' && can.isArray(dob.split('/')) && dob.split('/').length >= 2) {
            month = dob.split('/')[0];
            day = dob.split('/')[1];
            if(month !== '' && day !== '') {
                month = parseInt(month, 10);
                day = parseInt(day, 10);
                return { month: month, day: day };
            }
        }
        return null;
    }
});
