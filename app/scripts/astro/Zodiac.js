can.Construct('astro.Zodiac', {}, {

    signs: [
        "Aries",                        // 0
        "Aries/Taurus Cusp",            // 1
        "Taurus",                       // 2
        "Taurus/Gemini Cusp",           // 3
        "Gemini",                       // 4
        "Gemini/Cancer Cusp",           // 5
        "Cancer",                       // 6
        "Cancer/Leo Cusp",              // 7
        "Leo",                          // 8
        "Leo/Virgo Cusp",               // 9
        "Virgo",                        // 10
        "Virgo/Libra Cusp",             // 11
        "Libra",                        // 12
        "Libra/Scorpio Cusp",           // 13
        "Scorpio",                      // 14
        "Scorpio/Sagittarius Cusp",     // 15
        "Sagittarius",                  // 16
        "Sagittarius/Capricorn Cusp",   // 17
        "Capricorn",                    // 18
        "Capricorn/Aquarius Cusp",      // 19
        "Aquarius",                     // 20
        "Aquarius/Pisces Cusp",         // 21
        "Pisces",                       // 22
        "Pisces/Aries Cusp"             // 23
    ],

    getSign: function(month, day) {

        if(month >= 13 || month <= 0 || day >= 32 || day <= 0) {
            return null;
        } else if((month === 12 && day >= 19) && (month === 12 && day <= 23)) {
            return 17; // Sagittarius/Capricorn Cusp
        } else if((month === 1 && day <= 18) || (month === 12 && day >= 19)) {
            return 18; // Capricorn
        } else if((month === 1 && day >= 19) && (month === 1 && day <= 23)) {
            return 19; // Capricorn/Aquarius Cusp
        } else if ((month === 1 && day >= 24) || (month === 2 && day <= 18)) {
            return 20; // Aquarius
        } else if((month === 2 && day >= 19) && (month === 2 && day <= 23)) {
            return 21; // Aquarius/Pisces Cusp
        } else if((month === 2 && day >= 24) || (month === 3 && day <= 18)) {
            return 22; // Pisces
        } else if((month === 3 && day >= 19) && (month === 3 && day <= 23)) {
            return 23; // Pisces/Aries Cusp
        } else if((month === 3 && day >= 24) || (month === 4 && day <= 18)) {
            return 0; // Aries
        } else if((month === 4 && day >= 19) && (month === 4 && day <= 23)) {
            return 1; // Aries/Taurus Cusp
        } else if((month === 4 && day >= 24) || (month === 5 && day <= 18)) {
            return 2; // Taurus
        } else if((month === 5 && day >= 19) && (month === 5 && day <= 23)) {
            return 3; // Taurus/Gemini Cusp
        } else if((month === 5 && day >= 24) || (month === 6 && day <= 18)) {
            return 4; // Gemini
        } else if((month === 6 && day >= 19) && (month === 6 && day <= 23)) {
            return 5; // Gemini/Cancer Cusp
        } else if((month === 6 && day >= 24) || (month === 7 && day <= 18)) {
            return 6; // Cancer
        } else if((month === 7 && day >= 19) && (month === 7 && day <= 23)) {
            return 7; // Cancer/Leo Cusp
        } else if((month === 7 && day >= 24) || (month === 8 && day <= 18)) {
            return 8; // Leo
        } else if((month === 8 && day >= 19) && (month === 8 && day <= 23)) {
            return 9; // Leo/Virgo Cusp
        } else if((month === 8 && day >= 24) || (month === 9 && day <= 18)) {
            return 10; // Virgo
        } else if((month === 9 && day >= 19) && (month === 9 && day <= 23)) {
            return 11; // Virgo/Libra Cusp
        } else if((month === 9 && day >= 24) || (month === 10 && day <= 18)) {
            return 12; // Libra
        } else if((month === 10 && day >= 19) && (month === 10 && day <= 23)) {
            return 13; // Libra/Scorpio Cusp
        } else if((month === 10 && day >= 24) || (month === 11 && day <= 18)) {
            return 14; // Scorpio
        } else if((month === 11 && day >= 19) && (month === 11 && day <= 23)) {
            return 15; // Scorpio/Sagittarius Cusp
        } else if((month === 11 && day >= 24) || (month === 12 && day <= 21)) {
            return 16; // Sagittarius
        }
        return null;
    },

    getSignName: function(sign) {
        return this.signs[sign];
    }

});
