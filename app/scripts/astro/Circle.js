can.Construct('astro.Circle', {}, {
    numberOfSigns: 24,
    numberOfSpikes: 24,
    startX: 3000,
    startY: 3000,
    distanceBetweenCircles: 80,
    distanceFromCenterToFirstCircle: 230,
    _data: [],

    init: function() {
        this.initData();
    },

    initData: function() {
        this._data = [];
        for(var i = 0; i < this.numberOfSigns; i++) {
            this._data.push([ ]);
        }
    },

    spikeAngle: function() {
        return (Math.PI * 2) / this.numberOfSpikes;
    },

    /**
     * @param sign: number
     * @returns {{x: number, y: number}}
     */
    getSpot: function(sign) {
        if(sign < 0 || sign > this.numberOfSigns) {
            return null;
        }

        var alreadyInSign = this._data[sign].length,
            circleNumber,
            distanceFromCenter = this.distanceBetweenCircles * alreadyInSign + this.distanceFromCenterToFirstCircle,
            // subtract (Math.PI/2) to make sure Aries start at 0 (clock position)
            angle = this.spikeAngle() * sign - (Math.PI/2);

        if(alreadyInSign > 4) {
            circleNumber = 4 + ((alreadyInSign - 4) / 2);
            distanceFromCenter = this.distanceBetweenCircles * circleNumber + this.distanceFromCenterToFirstCircle;
            if(alreadyInSign % 2 === 1) {
                angle += this.spikeAngle() / 2;
                distanceFromCenter -= this.distanceBetweenCircles / 2;
            }
        }

        this._data[sign].push(1);

        return {
            x: distanceFromCenter * Math.cos(angle) + this.startX,
            y: distanceFromCenter * Math.sin(angle) + this.startY
        };
    }

});