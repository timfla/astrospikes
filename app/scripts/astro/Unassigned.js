can.Control('astro.Unassigned',
    {
        defaults: {
            view: '/scripts/astro/views/panel.mustache.js',
            data: []
        }
    },
    {
        init: function() {
            this.populateData(this.options.data);
        },

        populateData: function(data) {
            var observe, view, element = this.element.find('ul');

            can.each(data, function(item, index) {
                console.log('no dob', item);

                observe = new can.Observe({
                    name: item.name,
                    picture: item.picture.data.url
                });

                view = can.view('panel', observe);

                element.append(view);
            });
        }
    }
);