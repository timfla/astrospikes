

$(function() {
    window.sortedFriends = [],
    window.unassignedFriends = [];

    var birthday, dob, sign, signName, friendObserve,
        friendsList = $('.friends'),
        unassignedFriendsList = $('.unassigned'),
        zodiac = new astro.Zodiac(),
        circle = new astro.Circle(),
        utilFacebook = new astro.util.Facebook();


    // get data
    window.friends = window.fbData.friends.data;



    var count = 0;

    // determine the size of the canvas
    can.each(window.friends, function(friend) {
        birthday = friend.birthday;
        if(birthday) {
            dob = utilFacebook.extractDob(birthday);
            if(dob) {
                sign = zodiac.getSign(dob.month, dob.day);
                if(sign !== null) {
                    signName = zodiac.getSignName(sign);

                    $.extend(friend, {
                        sign:  sign,
                        signName: signName,
                        dob: dob
                    });

                    sortedFriends.push(friend);
                }
            }
        }
        else {
            unassignedFriends.push(friend);
        }
    });

    // process data
    sortedFriends = utilFacebook.sortByDob(sortedFriends);

    // display it
    can.each(sortedFriends, function(friend, i) {
        console.log('dob: ', i);
        count++;
        friendObserve = new can.Observe({
            name: '',//friend.name + ' - ' + friend.signName,
            picture: friend.picture.data.url,
            count: count
        }).attr(circle.getSpot(friend.sign));

        var view = can.view('circle', friendObserve);
        friendsList.append(view);
    });

    window.assigned = new astro.Unassigned('#unassigned', {
        data: unassignedFriends
    });


    $('.center').focus();

    return;

});



/*
    var Circles = can.Control({
        defaults: {
            view: 'circle',
            CIRCLES_PER_ROUND: 24,
            DISTANCE_BETWEEN_ROUNDS: 280,
            DISTANCE_BETWEEN_CIRCLES: 80
        }
    },{
        init: function(element, options) {

            $.each(options.friends, function(index, friend) {
                element.append(
                    can.view(options.view, {
                        id: friend.id,
                        name: friend.name,
                        birthday: friend.birthday,
                        picture: friend.picture.data.url
                    })
                );
            });

        },
        arrange: function() {

            console.log('arrange', this);

            var self = this,
                increase = Math.PI * 2 / this.options.CIRCLES_PER_ROUND,
                x = 0,
                y = 0,
                angle = 0,
                x_increase = 0,
                y_increase = 0;

            $('.friends li').each(function(index, li) {
                console.log(li, index, self);

                // new round
                if(index % self.options.CIRCLES_PER_ROUND == 0) {
                    if(index == 0) {
                        x_increase += 280;
                        y_increase += 280;
                        self.options.CIRCLES_PER_ROUND = 24;
                    }
                    else {
                        x_increase += 80;
                        y_increase += 80;
                        self.options.CIRCLES_PER_ROUND = 24;
                    }
                }

                x = x_increase * Math.cos(angle) + 3000;
                y = y_increase * Math.sin(angle) + 3000;

                li.style.position = 'absolute';
                li.style.left = x + 'px';
                li.style.top = y + 'px';

                angle += increase;

            });

        },
        "li click" : function(li){
            console.log(li);
            // li.trigger('selected', li.data('todo') );
            $('html,body').scrollTo(li, li, {
                gap: {
                    x: ($(window).width() / -2) + 20,
                    y: ($(window).height() / -2) + 50
                },
                animation: {
                    duration: 600,
                    complete: $.noop
                }
            });
        }

    });

    new Circles('.friends', { friends: fb_data.friends.data }).arrange();
    var center = $('.center');
    $('html,body').scrollTo(center, center, {
        gap: {
            x: ($(window).width() / -2) + 90,
            y: ($(window).height() / -2) + 100
        }
    });


});


*/