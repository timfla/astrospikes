module.exports = function(grunt) {
    var gruntConfig = {};

    // lint
    grunt.loadNpmTasks('grunt-contrib-jshint');
    gruntConfig.jshint = {
        all: [
            'app/scripts/*.js',
            'test/**/*.js'
        ]
    };

    // test
    grunt.loadNpmTasks('grunt-contrib-jasmine');
    gruntConfig.jasmine = {
        src: {
            src: [
                // vendor files (order matters)
                'app/scripts/vendor/jquery.2.0.1.js',
                'app/scripts/vendor/can.jquery.js',
                'app/scripts/vendor/can.view.mustache.js',

                // app files
                'app/scripts/astro/**/*.js'
            ],
            options: {
                specs: 'test/app/astro/**/*.test.js'
            }
        }
    };
    gruntConfig.jasmine.src.options.keepRunner = true;

    // grunt
    grunt.initConfig(gruntConfig);

};