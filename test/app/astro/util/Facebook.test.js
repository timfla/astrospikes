describe('astro.util.Facebook', function() {
    var facebook,
        friend1,
        friend2,
        friend3;

    beforeEach(function() {
        facebook = new astro.util.Facebook();
        friend1 = {
            dob: {
                day: 1,
                month: 1
            },
            sign: 0
        };
        friend2 = {
            dob: {
                day: 22,
                month: 2
            },
            sign: 2
        };
        friend3 = {
            dob: {
                day: 3,
                month: 3
            },
            sign: 2
        };


    });

    it('should extract DOB', function() {
        expect(facebook.extractDob('10/22')).toEqual({
            month: 10,
            day: 22
        });

        expect(facebook.extractDob('01/12')).toEqual({
            month: 1,
            day: 12
        });

        expect(facebook.extractDob('06/01')).toEqual({
            month: 6,
            day: 1
        });

        expect(facebook.extractDob('03/04/1978')).toEqual({
            month: 3,
            day: 4
        });

        expect(facebook.extractDob('02/18/1987')).toEqual({
            month: 2,
            day: 18
        });

        expect(facebook.extractDob('/22')).toBeNull();
        expect(facebook.extractDob('')).toBeNull();
        expect(facebook.extractDob(null)).toBeNull();
        expect(facebook.extractDob(12)).toBeNull();
    });

    it('should sort by sign and DOB', function() {
        var friends = [ friend3, friend2, friend1 ],
            sortedFriends = facebook.sortByDob(friends);
        expect(sortedFriends.length).toBe(3);
        expect(sortedFriends[0]).toEqual(friend1);
        expect(sortedFriends[1]).toEqual(friend2);
        expect(sortedFriends[2]).toEqual(friend3);
    });
});
