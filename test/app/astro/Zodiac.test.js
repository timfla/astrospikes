describe('astro.Zodiac', function() {

    var zodiac = new astro.Zodiac();

    it('should return Aries, Aries/Taurus Cusp, or Taurus (0,1,2)', function() {
        expect(zodiac.getSign(4, 17)).toBe(0);
        expect(zodiac.getSign(4, 18)).toBe(0);
        expect(zodiac.getSign(4, 19)).toBe(1);
        expect(zodiac.getSign(4, 20)).toBe(1);
        expect(zodiac.getSign(4, 21)).toBe(1);
        expect(zodiac.getSign(4, 22)).toBe(1);
        expect(zodiac.getSign(4, 23)).toBe(1);
        expect(zodiac.getSign(4, 24)).toBe(2);
        expect(zodiac.getSign(4, 25)).toBe(2);
    });

    it('should return Taurus, Taurus/Gemini Cusp, or Gemini (2,3,4)', function() {
        expect(zodiac.getSign(5, 17)).toBe(2);
        expect(zodiac.getSign(5, 18)).toBe(2);
        expect(zodiac.getSign(5, 19)).toBe(3);
        expect(zodiac.getSign(5, 20)).toBe(3);
        expect(zodiac.getSign(5, 21)).toBe(3);
        expect(zodiac.getSign(5, 22)).toBe(3);
        expect(zodiac.getSign(5, 23)).toBe(3);
        expect(zodiac.getSign(5, 24)).toBe(4);
        expect(zodiac.getSign(5, 25)).toBe(4);
    });

    it('should return Gemini, Gemini/Cancer Cusp, or Cancer (4,5,6)', function() {
        expect(zodiac.getSign(6, 17)).toBe(4);
        expect(zodiac.getSign(6, 18)).toBe(4);
        expect(zodiac.getSign(6, 19)).toBe(5);
        expect(zodiac.getSign(6, 20)).toBe(5);
        expect(zodiac.getSign(6, 21)).toBe(5);
        expect(zodiac.getSign(6, 22)).toBe(5);
        expect(zodiac.getSign(6, 23)).toBe(5);
        expect(zodiac.getSign(6, 24)).toBe(6);
        expect(zodiac.getSign(6, 25)).toBe(6);
    });

    it('should return Cancer, Cancer/Leo Cusp, or Leo (6,7,8)', function() {
        expect(zodiac.getSign(7, 17)).toBe(6);
        expect(zodiac.getSign(7, 18)).toBe(6);
        expect(zodiac.getSign(7, 19)).toBe(7);
        expect(zodiac.getSign(7, 20)).toBe(7);
        expect(zodiac.getSign(7, 21)).toBe(7);
        expect(zodiac.getSign(7, 22)).toBe(7);
        expect(zodiac.getSign(7, 23)).toBe(7);
        expect(zodiac.getSign(7, 24)).toBe(8);
        expect(zodiac.getSign(7, 25)).toBe(8);
    });

    it('should return Leo, Leo/Virgo Cusp, or Virgo (8, 9, 10)', function() {
        expect(zodiac.getSign(8, 17)).toBe(8);
        expect(zodiac.getSign(8, 18)).toBe(8);
        expect(zodiac.getSign(8, 19)).toBe(9);
        expect(zodiac.getSign(8, 20)).toBe(9);
        expect(zodiac.getSign(8, 21)).toBe(9);
        expect(zodiac.getSign(8, 22)).toBe(9);
        expect(zodiac.getSign(8, 23)).toBe(9);
        expect(zodiac.getSign(8, 24)).toBe(10);
        expect(zodiac.getSign(8, 25)).toBe(10);
    });

    it('should return Virgo, Virgo/Libra Cusp, or Libra (10,11,12)', function() {
        expect(zodiac.getSign(9, 17)).toBe(10);
        expect(zodiac.getSign(9, 18)).toBe(10);
        expect(zodiac.getSign(9, 19)).toBe(11);
        expect(zodiac.getSign(9, 20)).toBe(11);
        expect(zodiac.getSign(9, 21)).toBe(11);
        expect(zodiac.getSign(9, 22)).toBe(11);
        expect(zodiac.getSign(9, 23)).toBe(11);
        expect(zodiac.getSign(9, 24)).toBe(12);
        expect(zodiac.getSign(9, 25)).toBe(12);
    });

    it('should return Libra, Libra/Scorpio Cusp, or Scorpio (12,13,14)', function() {
        expect(zodiac.getSign(10, 17)).toBe(12);
        expect(zodiac.getSign(10, 18)).toBe(12);
        expect(zodiac.getSign(10, 19)).toBe(13);
        expect(zodiac.getSign(10, 20)).toBe(13);
        expect(zodiac.getSign(10, 21)).toBe(13);
        expect(zodiac.getSign(10, 22)).toBe(13);
        expect(zodiac.getSign(10, 23)).toBe(13);
        expect(zodiac.getSign(10, 24)).toBe(14);
        expect(zodiac.getSign(10, 25)).toBe(14);
    });

    it('should return Scorpio, Scorpio/Sagittarius Cusp, or Sagittarius (14,15,16)', function() {
        expect(zodiac.getSign(11, 17)).toBe(14);
        expect(zodiac.getSign(11, 18)).toBe(14);
        expect(zodiac.getSign(11, 19)).toBe(15);
        expect(zodiac.getSign(11, 20)).toBe(15);
        expect(zodiac.getSign(11, 21)).toBe(15);
        expect(zodiac.getSign(11, 22)).toBe(15);
        expect(zodiac.getSign(11, 23)).toBe(15);
        expect(zodiac.getSign(11, 24)).toBe(16);
        expect(zodiac.getSign(11, 25)).toBe(16);
    });

    it('should return Sagittarius, Sagittarius/Capricorn Cusp, or Capricorn (16,17,18)', function() {
        expect(zodiac.getSign(12, 17)).toBe(16);
        expect(zodiac.getSign(12, 18)).toBe(16);
        expect(zodiac.getSign(12, 19)).toBe(17);
        expect(zodiac.getSign(12, 20)).toBe(17);
        expect(zodiac.getSign(12, 21)).toBe(17);
        expect(zodiac.getSign(12, 22)).toBe(17);
        expect(zodiac.getSign(12, 23)).toBe(17);
        expect(zodiac.getSign(12, 24)).toBe(18);
        expect(zodiac.getSign(12, 25)).toBe(18);
    });

    it('should return Capricorn, Capricorn/Aquarius Cusp, or Aquarius (18,19,20)', function() {
        expect(zodiac.getSign(1, 17)).toBe(18);
        expect(zodiac.getSign(1, 18)).toBe(18);
        expect(zodiac.getSign(1, 19)).toBe(19);
        expect(zodiac.getSign(1, 20)).toBe(19);
        expect(zodiac.getSign(1, 21)).toBe(19);
        expect(zodiac.getSign(1, 22)).toBe(19);
        expect(zodiac.getSign(1, 23)).toBe(19);
        expect(zodiac.getSign(1, 24)).toBe(20);
        expect(zodiac.getSign(1, 25)).toBe(20);
    });

    it('should return Aquarius, Aquarius/Pisces Cusp, or Pisces (20,21,22)', function() {
        expect(zodiac.getSign(2, 17)).toBe(20);
        expect(zodiac.getSign(2, 18)).toBe(20);
        expect(zodiac.getSign(2, 19)).toBe(21);
        expect(zodiac.getSign(2, 20)).toBe(21);
        expect(zodiac.getSign(2, 21)).toBe(21);
        expect(zodiac.getSign(2, 22)).toBe(21);
        expect(zodiac.getSign(2, 23)).toBe(21);
        expect(zodiac.getSign(2, 24)).toBe(22);
        expect(zodiac.getSign(2, 25)).toBe(22);
    });

    it('should return Pisces, Pisces/Aries Cusp, or Aries (22,23,24)', function() {
        expect(zodiac.getSign(3, 17)).toBe(22);
        expect(zodiac.getSign(3, 18)).toBe(22);
        expect(zodiac.getSign(3, 19)).toBe(23);
        expect(zodiac.getSign(3, 20)).toBe(23);
        expect(zodiac.getSign(3, 21)).toBe(23);
        expect(zodiac.getSign(3, 22)).toBe(23);
        expect(zodiac.getSign(3, 23)).toBe(23);
        expect(zodiac.getSign(3, 24)).toBe(0);
        expect(zodiac.getSign(3, 25)).toBe(0);
    });

    it('should return null', function() {
        expect(zodiac.getSign(23, -22)).toBeNull();
        expect(zodiac.getSign(1, -1)).toBeNull();
        expect(zodiac.getSign(0, 12)).toBeNull();
        expect(zodiac.getSign(1, 32)).toBeNull();
        expect(zodiac.getSign(13, 13)).toBeNull();
        expect(zodiac.getSign('a', 23)).toBeNull();
        expect(zodiac.getSign(1, 'b')).toBeNull();
    });

    it('should return sign name: Sagittarius', function() {
        expect(zodiac.getSignName(16)).toBe('Sagittarius');
    });

    it('should return sign name: Libra', function() {
        expect(zodiac.getSignName(12)).toBe('Libra');
    });

    it('should return sign name: Libra/Scorpio Cusp', function() {
        expect(zodiac.getSignName(13)).toBe('Libra/Scorpio Cusp');
    });

    it('should return sign name: Aries/Taurus Cusp', function() {
        expect(zodiac.getSignName(1)).toBe('Aries/Taurus Cusp');
    });

});