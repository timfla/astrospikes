describe('astro.Circle', function() {
    var circle;

    beforeEach(function() {
        spyOn(astro.Circle.prototype, 'initData').andCallThrough();

        circle = new astro.Circle();
    });


    it('should initialize data', function() {
        expect(astro.Circle.prototype.initData).toHaveBeenCalled();
    });

    it('should initialize correctly', function() {
        expect(circle._data).toEqual([ [],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[],[] ]);
    });

    describe('getSpot', function() {
        it('should return null if sign is less then 0 or more than 12', function() {
            expect(circle.getSpot(-1)).toBeNull();
            expect(circle.getSpot(26)).toBeNull();
            expect(circle.getSpot(-2)).toBeNull();
            expect(circle.getSpot(34)).toBeNull();
            expect(circle.getSpot(3)).not.toBeNull();
            expect(circle.getSpot(4)).not.toBeNull();
            expect(circle.getSpot(0)).not.toBeNull();
        });

        it('should save spots in data', function() {
            circle.getSpot(1);
            expect(circle._data[1].length).toEqual(1);

            circle.getSpot(1);
            circle.getSpot(1);
            expect(circle._data[1].length).toEqual(3);

            circle.getSpot(2);
            circle.getSpot(2);
            expect(circle._data[2].length).toEqual(2);

            circle.getSpot(3);
            circle.getSpot(4);
            expect(circle._data[1].length).toEqual(3);
            expect(circle._data[2].length).toEqual(2);
        });
    });

});
